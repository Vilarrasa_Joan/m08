<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Entity\UserManager;
use AppBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/loginRedirect", name="redirect")
     */
    public function indexAction(Request $request)
    {
        $authChecker = $this->container->get('security.authorization_checker');
        if ($authChecker->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_home');
        }
        if ($authChecker->isGranted('ROLE_CAP')) {
            return $this->redirectToRoute('cap_home');
        }
        if ($authChecker->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('user_home');
        }
        return $this->redirect('/login');
    }

    /**
     * @Route("/admin/home", name="admin_home")
     */
    public function adminIndexAction(Request $request)
    {
        $authChecker = $this->container->get('security.authorization_checker');

        if ($authChecker->isGranted('ROLE_ADMIN')) {
            $users = $this->getDoctrine()->getRepository(User::class)->findAll();
            return $this->render('default/home.html.twig', [
                'users' => $users,
            ]);
        }

        return new RedirectResponse($router->generate('logout'), 307);
    }

    /**
     * @Route("/cap/home", name="cap_home")
     */
    public function capIndexAction(Request $request)
    {
        $authChecker = $this->container->get('security.authorization_checker');;

        if ($authChecker->isGranted('ROLE_CAP')) {
            $us = $this->getDoctrine()->getRepository(User::class)->findAll();
            $users = [];
            foreach ($us as $user){
                if($user->getRoles() == ['ROLE_CAP', 'ROLE_USER'] || $user->getRoles() == ['ROLE_USER'])
                    array_push($users, $user);
            }
            return $this->render('default/home.html.twig', [
                'users' => $users,
            ]);
        }

        return new RedirectResponse($router->generate('logout'), 307);
    }

    /**
     * @Route("/home", name="user_home")
     */
    public function userIndexAction(Request $request)
    {
        $authChecker = $this->container->get('security.authorization_checker');

        if ($authChecker->isGranted('ROLE_USER')) {
            $us = $this->getDoctrine()->getRepository(User::class)->findAll();
            $users = [];
            foreach ($us as $user){
                if($user->getRoles() == ['ROLE_USER'])
                    array_push($users, $user);
            }
            return $this->render('default/home.html.twig', [
                'users' => $users,
            ]);
        }

        return new RedirectResponse($router->generate('logout'), 307);
    }
}
