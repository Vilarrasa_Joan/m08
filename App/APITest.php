<!DOCTYPE html>
<?php
require_once 'Routes.php';
$preguntes = callApi('GET', 'preguntes');
$preguntes = json_decode($preguntes);
?>
<html>
    <head>
        <meta charset="utf-8" />
        <title>FOSRestBundleTest</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="main.css"/>
        <script src="vendor/components/jquery/jquery.min.js"></script>
        <script src="main.js"></script>
    </head>
    <body>
        <h1>Preguntes de resposta múltiple</h1><button id="b">Mostrar respostes</button>
        <ul>
            <?php foreach($preguntes as $pregunta) { ?>
            <li>
                <div><span class="pregunta"><?php echo $pregunta->preg; ?></span></div>
                <div class="resp_encap">
                    <?php foreach($pregunta->respostes as $resposta) { ?>
                        <?php if($resposta->correcte) { ?>
                            <span class="resposta r"><?php echo $resposta->resp; ?></span>
                        <?php }else{ ?>
                            <span class="resposta w"><?php echo $resposta->resp; ?></span>
                        <?php } ?>
                    <?php } ?>
                </div>
            </li>
            <?php } ?>
        </ul>
    </body>
</html>
