<?php

function CallAPI($method, $url, $data = false)
{
    $ch = curl_init();
    if($method == 'GET') {
        curl_setopt_array(
            $ch, array(
            CURLOPT_URL => 'http://localhost:8000/' . $url,
            CURLOPT_RETURNTRANSFER => true,
        ));
    }
    elseif($method == 'POST') {
        curl_setopt_array(
            $ch, array(
            CURLOPT_URL => 'http://localhost:8000/' . $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => array(data)
        ));
    }

    $output = curl_exec($ch);
    curl_close($ch);

    return $output;
}
