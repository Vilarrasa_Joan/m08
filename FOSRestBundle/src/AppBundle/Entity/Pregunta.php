<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Pregunta
 *
 * @ORM\Table(name="pregunta")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PreguntaRepository")
 */
class Pregunta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="preg", type="string", length=255)
     */
    private $preg;

    /**
     * @ORM\OneToMany(targetEntity="Resposta", mappedBy="pregunta")
     */
    private $respostes;

    public function __construct()
    {
        $this->respostes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set preg
     *
     * @param string $preg
     *
     * @return Pregunta
     */
    public function setPreg($preg)
    {
        $this->preg = $preg;

        return $this;
    }

    /**
     * Get preg
     *
     * @return string
     */
    public function getPreg()
    {
        return $this->preg;
    }
}
