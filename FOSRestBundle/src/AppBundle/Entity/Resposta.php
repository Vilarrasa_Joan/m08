<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resposta
 *
 * @ORM\Table(name="resposta")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RespostaRepository")
 */
class Resposta
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="resp", type="string", length=255)
     */
    private $resp;

    /**
     * @var bool
     *
     * @ORM\Column(name="correcte", type="boolean")
     */
    private $correcte;

    /**
     * @ORM\ManyToOne(targetEntity="Pregunta", inversedBy="respostes")
     * @ORM\JoinColumn(name="pregunta_id", referencedColumnName="id")
     */
    private $pregunta;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set resp
     *
     * @param string $resp
     *
     * @return Resposta
     */
    public function setResp($resp)
    {
        $this->resp = $resp;

        return $this;
    }

    /**
     * Get resp
     *
     * @return string
     */
    public function getResp()
    {
        return $this->resp;
    }

    /**
     * Set correcte
     *
     * @param boolean $correcte
     *
     * @return Resposta
     */
    public function setCorrecte($correcte)
    {
        $this->correcte = $correcte;

        return $this;
    }

    /**
     * Get correcte
     *
     * @return bool
     */
    public function getCorrecte()
    {
        return $this->correcte;
    }

    /**
     * Set pregunta
     *
     * @param boolean $pregunta
     *
     * @return Resposta
     */
    public function setPregunta($pregunta)
    {
        $this->pregunta = $pregunta;

        return $this;
    }

    /**
     * Get pregunta
     *
     * @return bool
     */
    public function getPregunta()
    {
        return $this->pregunta;
    }

}
