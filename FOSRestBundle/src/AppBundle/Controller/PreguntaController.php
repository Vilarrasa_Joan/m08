<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Pregunta;

class PreguntaController extends FOSRestController
{
    /**
     * @Rest\Get("/preguntes")
     */
    public function getAllAction()
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Pregunta')->findAll();
        if ($restresult === null) {
            return new View("No hi ha preguntes a la BBDD", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/pregunta/{id}")
     */
    public function getIdAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Pregunta')->find($id);
        if ($singleresult === null) {
            return new View("Pregunta no trobada", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @Rest\Post("/pregunta/")
     */
    public function postAction(Request $request)
    {
        $data = new Pregunta;
        $preg = $request->get('pregunta');
        if(empty($preg))
        {
            return new View("No es permeten valors nulls", Response::HTTP_NOT_ACCEPTABLE);
        }
        $data->setPreg($preg);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View("Pregunta afegida correctament", Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/pregunta/{id}")
     */
    public function updateAction($id,Request $request)
    {
        $data = new Pregunta;
        $preg = $request->get('pregunta');
        $sn = $this->getDoctrine()->getManager();
        $pregunta = $this->getDoctrine()->getRepository('AppBundle:Pregunta')->find($id);
        if (empty($pregunta)) {
            return new View("Pregunta no trobada", Response::HTTP_NOT_FOUND);
        }
        elseif(!empty($preg)) {
            $pregunta->setPreg($preg);
            $sn->flush();
            return new View("Pregunta actualitzada correctament", Response::HTTP_OK);
        }
        else return new View("No es permeten valors nulls", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @Rest\Delete("/pregunta/{id}")
     */
    public function deleteAction($id)
    {
        $data = new Pregunta;
        $sn = $this->getDoctrine()->getManager();
        $pregunta = $this->getDoctrine()->getRepository('AppBundle:Pregunta')->find($id);
        if (empty($pregunta)) {
            return new View("Pregunta no trobada", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($pregunta);
            $sn->flush();
        }
        return new View("Pregunta eliminada correctament", Response::HTTP_OK);
    }

}

?>
