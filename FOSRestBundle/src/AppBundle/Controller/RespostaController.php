<?php
namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\View\View;
use AppBundle\Entity\Pregunta;
use AppBundle\Entity\Resposta;

class RespostaController extends FOSRestController
{
    /**
     * @Rest\Get("/respostes")
     */
    public function getAllAction()
    {
        $restresult = $this->getDoctrine()->getRepository('AppBundle:Resposta')->findAll();
        if ($restresult === null) {
            return new View("No hi ha respostes a la BBDD", Response::HTTP_NOT_FOUND);
        }
        return $restresult;
    }

    /**
     * @Rest\Get("/resposta/{id}")
     */
    public function getIdAction($id)
    {
        $singleresult = $this->getDoctrine()->getRepository('AppBundle:Resposta')->find($id);
        if ($singleresult === null) {
            return new View("Resposta no trobada", Response::HTTP_NOT_FOUND);
        }
        return $singleresult;
    }

    /**
     * @Rest\Post("/resposta/")
     */
    public function postAction(Request $request)
    {
        $data = new Resposta;
        $resp = $request->get('resposta');
        $correcte = $request->get('correcte');
        $preguntaId = $request->get('preguntaId');
        $pregunta = $this->getDoctrine()->getRepository('AppBundle:Pregunta')->find($preguntaId);
        if(empty($resp) || empty($preguntaId))
        {
            return new View("No es permeten valors nulls", Response::HTTP_NOT_ACCEPTABLE);
        }
        if(empty($correcte))
            $correcte = false;
        $data->setResp($resp);
        $data->setCorrecte($correcte);
        $data->setPregunta($pregunta);
        $em = $this->getDoctrine()->getManager();
        $em->persist($data);
        $em->flush();
        return new View("Resposta afegida correctament", Response::HTTP_OK);
    }

    /**
     * @Rest\Put("/resposta/{id}")
     */
    public function updateAction($id,Request $request)
    {
        $data = new Resposta;
        $resp = $request->get('resposta');
        $sn = $this->getDoctrine()->getManager();
        $resposta = $this->getDoctrine()->getRepository('AppBundle:Resposta')->find($id);
        if (empty($resp)) {
            return new View("No es permeten valors nulls", Response::HTTP_NOT_FOUND);
        }
        elseif(!empty($resp)) {
            $resposta->setResp($resp);
            $sn->flush();
            return new View("Resposta actualitzada correctament", Response::HTTP_OK);
        }
        else return new View("No es permeten valors nulls", Response::HTTP_NOT_ACCEPTABLE);
    }

    /**
     * @Rest\Delete("/resposta/{id}")
     */
    public function deleteAction($id)
    {
        $data = new Resposta;
        $sn = $this->getDoctrine()->getManager();
        $resposta = $this->getDoctrine()->getRepository('AppBundle:Resposta')->find($id);
        if (empty($resposta)) {
            return new View("Resposta no trobada", Response::HTTP_NOT_FOUND);
        }
        else {
            $sn->remove($resposta);
            $sn->flush();
        }
        return new View("Resposta eliminada correctament", Response::HTTP_OK);
    }

}

?>
